import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZorbeesKitchenComponent } from './zorbees-kitchen.component';

describe('ZorbeesKitchenComponent', () => {
  let component: ZorbeesKitchenComponent;
  let fixture: ComponentFixture<ZorbeesKitchenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZorbeesKitchenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZorbeesKitchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
